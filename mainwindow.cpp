#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString xToString(QVector<double> roots) {
    QString text = "result:\n";
    for (int i = 0; i < roots.size(); i++) {
        text += "x" + QString::number(i+1) + " = " + QString::number(roots[i]) + ",\n";
    }
    return text;
}

void MainWindow::on_pushButton_eq1_clicked()
{
    double a = ui->doubleSpinBox_eq1_a->value();
    double b = ui->doubleSpinBox_eq1_b->value();

    QString text = "x = " + QString::number(equation1(a, b));

    ui->plainTextEdit->setPlainText(text);
}

void MainWindow::on_pushButton_eq2_clicked()
{
    double a = ui->doubleSpinBox_eq2_a->value();
    double b = ui->doubleSpinBox_eq2_b->value();
    double c = ui->doubleSpinBox_eq2_c->value();

    QString text = xToString(equation2(a, b, c));

    ui->plainTextEdit->setPlainText(text);
}

void MainWindow::on_pushButton_eq3_clicked()
{
    double a = ui->doubleSpinBox_eq3_a->value();
    double b = ui->doubleSpinBox_eq3_b->value();
    double c = ui->doubleSpinBox_eq3_c->value();
    double d = ui->doubleSpinBox_eq3_d->value();

    QString text = xToString(equation3(a, b, c, d));

    ui->plainTextEdit->setPlainText(text);
}

void MainWindow::on_pushButton_pr1_clicked()
{
    double a = ui->doubleSpinBox_pr1_a->value();
    double d = ui->doubleSpinBox_pr1_d->value();
    double n = ui->doubleSpinBox_pr1_n->value();

    QString text = "i = " + QString::number(iOfAProgression(a, d, n));

    ui->plainTextEdit->setPlainText(text);
}

void MainWindow::on_pushButton_pr2_clicked()
{
    double a = ui->doubleSpinBox_pr2_a->value();
    double d = ui->doubleSpinBox_pr2_d->value();
    double n = ui->doubleSpinBox_pr2_n->value();

    QString text = "sum = " + QString::number(sumOfAProgression(a, d, n));

    ui->plainTextEdit->setPlainText(text);
}

void MainWindow::on_pushButton_pr3_clicked()
{
    double a = ui->doubleSpinBox_pr3_a->value();
    double q = ui->doubleSpinBox_pr3_q->value();
    double n = ui->doubleSpinBox_pr3_n->value();

    QString text = "sum = " + QString::number(sumOfGProgression(a, q, n));

    ui->plainTextEdit->setPlainText(text);
}
