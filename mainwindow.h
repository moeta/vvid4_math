/// \file mainwindow.h
/// Mainwindow of project.
///

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    /// @brief Execute solving of equation
    void on_pushButton_eq1_clicked();

    /// @brief Execute solving of equation
    void on_pushButton_eq2_clicked();

    /// @brief Execute solving of equation
    void on_pushButton_eq3_clicked();

    /// @brief Execute solving of progression
    void on_pushButton_pr1_clicked();

    /// @brief Execute solving of progression
    void on_pushButton_pr2_clicked();

    /// @brief Execute solving of progression
    void on_pushButton_pr3_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
