#include "functions.h"
#include <QVector>
#include <QtMath>

double equation1(double a, double b) {
    return -b / a;
}

QVector<double> equation2(double a, double b, double c) {
    double x1, x2;
    QVector<double> result;

    double d = b * b - 4 * a * c;

    if (d > 0)
    {
        x1 = (-b + sqrt(d)) / (2*a);
        x2 = (-b - sqrt(d)) / (2*a);

        result.push_back(x1);
        result.push_back(x2);
    }
    else
    {
        x1 = (-b) / (2*a);

        result.push_back(x1);
    }

    return result;
}

QVector<double> equation3(double a, double b, double c, double d) {
    double x1, x2, x3;

    b /= a;
    c /= a;
    d /= a;
    double disc, q, r, dum1, s, t, term1, r13;
    q = (3.0*c - (b*b))/9.0;
    r = -(27.0*d) + b*(9.0*c - 2.0*(b*b));
    r /= 54.0;
    disc = q*q*q + r*r;
    x1 = 0; //The first root is always real.
    term1 = (b/3.0);
    if (disc > 0) { // one root real, two are complex
        s = r + sqrt(disc);
        s = ((s < 0) ? -qPow(-s, (1.0/3.0)) : qPow(s, (1.0/3.0)));
        t = r - sqrt(disc);
        t = ((t < 0) ? -qPow(-t, (1.0/3.0)) : qPow(t, (1.0/3.0)));
        x1 = -term1 + s + t;
        term1 += (s + t)/2.0;
        x3 = -term1;
        x2 = -term1;
        term1 = sqrt(3.0)*(-t + s)/2;
        x2 = term1;
        x3 = -term1;
        return {x1, x2, x3};
    }

    x3 = 0;
    x2 = 0;
    if (disc == 0){ // All roots real, at least two are equal.
        r13 = ((r < .0) ? -qPow(-r,(1.0/3.0)) : qPow(r,(1.0/3.0)));
        x1 = -term1 + 2.0*r13;
        x3 = -(r13 + term1);
        x2 = -(r13 + term1);
        return {x1, x2, x3};
    }

    q = -q;
    dum1 = q*q*q;
    dum1 = qAcos(r/sqrt(dum1));
    r13 = 2.0*sqrt(q);
    x1 = -term1 + r13*qCos(dum1/3.0);
    x2 = -term1 + r13*qCos((dum1 + 2.0*M_PI)/3.0);
    x3 = -term1 + r13*qCos((dum1 + 4.0*M_PI)/3.0);
    return {x1, x2, x3};
}

float iOfAProgression(float a1, float d, float n) {
    return a1 + d*(n-1);
}

float sumOfAProgression(float a1, float d, float n) {
    return (2*a1 + d*(n-1))/2*n;
}

float sumOfGProgression(float b1, float q, float n) {
    if (q < 1 && q > -1)
    {
        return b1 / (1-q);
    }
    else
    {
        return b1*(1 - qPow(q, n))/(1-q);
    }
}
