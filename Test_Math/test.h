#ifndef TEST_H
#define TEST_H

#include <QtTest/QtTest>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = nullptr);

signals:

private slots:
    void test1();
    void test2();
    void test3();
    void test4();
    void test5();
    void test6();
};

#endif // TEST_H
