#include "test.h"
#include "../functions.h"

Test::Test(QObject *parent) : QObject(parent)
{

}

bool compareArr(QVector<double> &arr1, QVector<double> &arr2) {
    for (int i = 0; i < arr2.size(); i++) {
        if (QString::number(arr1[i]) != QString::number(arr2[i])) return false;
    }
    return true;
}

void Test::test1()
{
    double a = 1;
    double b = 1;
    QCOMPARE(-1, equation1(a, b));

    a = 1;
    b = 0;
    QCOMPARE(0, equation1(a, b));

    a = -1;
    b = 2;
    QCOMPARE(2, equation1(a, b));
}

void Test::test2()
{
    double a = 1;
    double b = 2;
    double c = 1;
    QVector<double> arr1 = equation2(a, b, c);
    QVector<double> arr2 = {-1};
    QCOMPARE(true, compareArr(arr1, arr2));

    a = 1;
    b = -2;
    c = 0;
    arr1 = equation2(a, b, c);
    arr2 = {2, 0};
    QCOMPARE(true, compareArr(arr1, arr2));

    a = 1;
    b = 0;
    c = 0;
    arr1 = equation2(a, b, c);
    arr2 = {0};
    QCOMPARE(true, compareArr(arr1, arr2));
}

void Test::test3()
{
    double a = 2;
    double b = -4;
    double c = -22;
    double d = 24;
    QVector<double> arr1 = equation3(a, b, c, d);
    QVector<double> arr2 = {4, -3, 1};
    QCOMPARE(true, compareArr(arr1, arr2));

    a = 1;
    b = 6;
    c = 12;
    d = 8;
    arr1 = equation3(a, b, c, d);
    arr2 = {-2, -2, -2};
    QCOMPARE(true, compareArr(arr1, arr2));

    a = 1;
    b = 0;
    c = 0;
    d = 0;
    arr1 = equation3(a, b, c, d);
    arr2 = {0, 0, 0};
    QCOMPARE(true, compareArr(arr1, arr2));
}

void Test::test4()
{
    double a = 0;
    double d = 1;
    double n = 5;
    QCOMPARE(4, iOfAProgression(a, d, n));

    a = 1;
    d = -1;
    n = 11;
    QCOMPARE(-9, iOfAProgression(a, d, n));

    a = 0;
    d = 0.5;
    n = 6;
    QCOMPARE(2.5, iOfAProgression(a, d, n));
}

void Test::test5()
{
    double a = 1;
    double d = 1;
    double n = 6;
    QCOMPARE(21, sumOfAProgression(a, d, n));

    a = 1;
    d = -2;
    n = 6;
    QCOMPARE(-24, sumOfAProgression(a, d, n));

    a = 0;
    d = -0.5;
    n = 6;
    QCOMPARE(-7.5, sumOfAProgression(a, d, n));
}

void Test::test6()
{
    double a = 1;
    double d = 2;
    double n = 3;
    QCOMPARE(7, sumOfGProgression(a, d, n));

    a = 1;
    d = -2;
    n = 3;
    QCOMPARE(3, sumOfGProgression(a, d, n));

    a = 1;
    d = 0.5;
    n = 3;
    QCOMPARE(2, sumOfGProgression(a, d, n));
}

//QTEST_MAIN(Test);
