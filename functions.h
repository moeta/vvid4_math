/// \file functions.h
/// Functions of the project.
///


#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>


/// @brief Solve simple equation | Ax + B = 0.
/// @param a parameter.
/// @param b parameter.
/// @return The unknown of equation.
double equation1(double a, double b);

/// @brief Solve square equation | Ax^2 + Bx + C = 0.
/// @param a parameter.
/// @param b parameter.
/// @param c parameter.
/// @return The vector of unknowns of equation.
QVector<double> equation2(double a, double b, double c);

/// @brief Solve square equation | Ax^# + Bx^2 + Cx + D = 0.
/// @param a parameter.
/// @param b parameter.
/// @param c parameter.
/// @param d parameter.
/// @return The vector of unknowns of equation.
QVector<double> equation3(double a, double b , double c, double d);

/// @brief Get i element of arithmetic progression.
/// @param a1 first element.
/// @param d step.
/// @param n number of elements.
/// @return The number of element.
float iOfAProgression(float a1, float d, float n);

/// @brief Get sum of arithmetic progression elements.
/// @param a1 first element.
/// @param d step.
/// @param n number of elements.
/// @return The sum of arithmetic progression.
float sumOfAProgression(float a1, float d, float n);

/// @brief Get sum of geometric progression elements.
/// @param b1 first element.
/// @param q step.
/// @param n number of elements.
/// @return The sum of geometric progression.
float sumOfGProgression(float b1, float q, float n);

#endif // FUNCTIONS_H
